angular.module('materialAdmin').controller("CotacaoController",CotacaoController);


function CotacaoController($scope){
	$scope.teste = "Cotacao";

	$scope.status = true;
	$scope.usuario = true;
	$scope.criacao = true;
	$scope.prazo_resposta = false;
	$scope.arquivo = true;
	$scope.resposta = true;
	$scope.ren = false;

	$scope.objeto = [
		{
			status:'C',
			nome:'Teste',
			itens:5,
			criacao:'27/08/2016',
			prazo_resposta:'27/08/2016',
			usuario:'suporte',
			resposta:'nao enviada'
		},
		{
			status:'P',
			nome:'Teste 2',
			itens:5,
			criacao:'27/08/2016',
			prazo_resposta:'27/08/2016',
			usuario:'suporte',
			resposta:'3'
		},
		{
			status:'A',
			nome:'Teste 3',
			itens:5,
			criacao:'27/08/2016',
			prazo_resposta:'27/08/2016',
			usuario:'suporte',
			resposta:'nao enviada'
		}
	];

	$scope.go = function(name){
		console.log(name);
	}
};