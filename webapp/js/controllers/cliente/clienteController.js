angular.module('materialAdmin').controller("ClienteController",function($scope, $http){

  $http.get('http://localhost:8000/teste', function (data) {
      console.log(data);
  });

  $scope.nome = "Edita Cliente";
  $scope.areasDeAtuacao = ['abatedouro','academia'];
  $scope.cliente = {
    cnpj:'01.551.272/0001-42',
    razao_social:"ASA INDUSTRIA E COMERCIO LTDA",
    nome_fantasia:"ASA INDUSTRIA E COMERCIO LTDA",
    pais:"Brasil",
    estado:"Pernambuco",
    cidade:"Recife",
    endereco:"Rua da Paz",
    complemento:"82",
    bairro:"Afogados",
    cep:"50770-000",
    inscricao_estadual:{
      numero:022905707,
      isento:false
    },
    inscricao_municipal:{
      numero:022905707,
      isento:false
    },
    telefone:"81 30735000",
    ramal:null,
    home_page:null,
    atuacao:[]
  }

  $scope.tabs = [
    {id:1,title:"Cadastro do Cliente"},
    {id:2,title:"Área de Atuação",content:"check"}
  ];

  $scope.isFormCadastro = function(tab){
    if(tab.id == 1){
      return true;
    }
  }


});
