materialAdmin
    .config(function ($stateProvider, $urlRouterProvider){

        $urlRouterProvider.otherwise("/login");



        $stateProvider

            .state('login',{
                url: '/login',
                templateUrl: 'login.html',
                controller: 'loginCtrl as vm'
            })


            //------------------------------
            // HOME
            //------------------------------
            .state ('home', {
                url: '/home',
                templateUrl: 'views/home/home.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/fullcalendar/dist/fullcalendar.min.css',
                                ]
                            },
                            {
                                name: 'vendors',
                                insertBefore: '#app-level-js',
                                files: [
                                    'vendors/sparklines/jquery.sparkline.min.js',
                                    'vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
                                ]
                            }
                        ])
                    }
                }
            })

            //------------------------------
            // MINHAS NEGOCIAÇÕES
            //------------------------------

            .state ('negociacoes', {
                url: '/negociacoes',
                templateUrl: 'views/common-2.html'
            })

            .state ('negociacoes.gerencia-fornecedor', {
                url: '/gerencia-fornecedor',
                templateUrl: 'views/minhasNegociacoes/gerencia-fornecedor.html'
            })
            .state ('negociacoes.gerencia-requisicoes', {
                url: '/gerencia-requisicoes',
                templateUrl: 'views/minhasNegociacoes/gerencia-requisicoes.html'
            })
            .state ('negociacoes.gerencia-cotacoes', {
                url: '/gerencia-cotacoes',
                controller: 'CotacaoController',
                templateUrl: 'views/minhasNegociacoes/cotacao/gerencia-cotacoes.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                insertBefore: '#app-level-js',
                                files: [
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js',
                                    'js/controllers/cotacao/cotacaoController.js'
                                ]
                            }
                        ])
                    }
                }
            })
            .state ('nova-cotacao', {
                url: '/nova-cotacao',
                controller: 'NovaCotacaoController',
                templateUrl: 'views/minhasNegociacoes/cotacao/nova-cotacao.html',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'vendors',
                                insertBefore: '#app-level-js',
                                files: [
                                    'js/controllers/cotacao/novaCotacaoController.js'
                                ]
                            }
                        ])
                    }
                }
            })
            .state ('negociacoes.gerencia-pedido', {
                url: '/gerencia-pedido',
                templateUrl: 'views/minhasNegociacoes/gerencia-pedido.html'                
            })


            //------------------------------
            // INTEGRACAO
            //------------------------------

            .state ('integracao', {
                url: '/integracao',
                templateUrl: 'views/integracao/integracao.html'
            })

            //------------------------------
            // HISTORICO DE FATURAS
            //------------------------------

            .state ('historico', {
                url: '/historico-faturas',
                templateUrl: 'views/historico/historico-faturas.html'
            })

            //------------------------------
            // CONSULTA
            //------------------------------

            .state ('consulta', {
                url: '/nova-consulta',
                templateUrl: 'views/consulta/nova-consulta.html'
            })

            //------------------------------
            // FALE CONOSCO
            //------------------------------

            .state ('faleconosco', {
                url: '/faleconosco',
                templateUrl: 'views/faleconosco/faleconosco.html'
            })

    });
