materialAdmin
.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
    //------------------------------
    // Gerencia de usuario
    //------------------------------
        .state ('cliente', {
            url: '/cliente',
            templateUrl: 'views/common-2.html',
        })
        .state ('cliente.profile', {
            url: '/profile',
            templateUrl: 'views/cliente/index.html',
            controller:'ClienteCtrl',
            resolve: {
                loadPlugin: function($ocLazyLoad) {
                    return $ocLazyLoad.load ([
                        {
                            name: 'vendors',
                            insertBefore: '#app-level-js',
                            files: [
                                'js/app/modulos/cliente/clienteController.js',
                                'js/app/modulos/cliente/clienteFactory.js'
                            ]
                        }
                    ])
                }
            }
        })
        .state ('cliente.profile.perfil', {
            url: '/id',
            templateUrl: 'views/cliente/profile.html'
        })
        .state ('cliente.profile.usuarios', {
            url: '/usuarios',
            templateUrl: 'views/cliente/usuarios.html',
            controller:'ClienteCtrl as vm',
        })
        .state ('cliente.profile.usuario', {
            url: '/usuario/:id',
            templateUrl: 'views/cliente/editar-usuario.html',
            controller:'UsuarioCtrl as vm',
            resolve: {
                loadPlugin: function($ocLazyLoad) {
                    return $ocLazyLoad.load ([
                        {
                            name: 'vendors',
                            insertBefore: '#app-level-js',
                            files: [
                                'js/app/modulos/cliente/usuarioController.js',
                                'js/app/modulos/cliente/usuarioFactory.js'
                            ]
                        }
                    ])
                }
            }
        })

        .state('cliente.profile.novousuario', {
            url: '/novo-usuario',
            templateUrl: 'views/cliente/novo-usuario.html',
            controller:'UsuarioCtrl',
            controllerAs: 'vm',
            resolve: {
                loadPlugin: function($ocLazyLoad) {
                    return $ocLazyLoad.load ([
                        {
                            name: 'vendors',
                            insertBefore: '#app-level-js',
                            files: [
                                'js/app/modulos/cliente/usuarioController.js',
                                'js/app/modulos/cliente/usuarioFactory.js'
                            ]
                        }
                    ])
                }
            }
        })

        .state('cliente.edita-usuario', {
            url: '/edita-usuario/:id',
            templateUrl: 'views/cliente/edita-usuario.html',
            controller:'UsuarioController',
            resolve: {
                loadPlugin: function($ocLazyLoad) {
                    return $ocLazyLoad.load ([
                        {
                            name: 'vendors',
                            insertBefore: '#app-level-js',
                            files: [
                                'js/controllers/cliente/usuarioController.js'
                            ]
                        }
                    ])
                }
            }
        })
        .state('cliente.definir-permissoes', {
            url: '/definir-permissoes',
            templateUrl: 'views/cliente/definir-permissoes.html'
        })

});
