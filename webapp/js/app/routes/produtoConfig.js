angular.module('materialAdmin')
    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state ('produtos', {
                templateUrl: 'views/common-2.html'
            })
            .state ('produtos.main', {
                url: '/produtos',
                templateUrl: 'views/produtos/index.html',
                //controller: 'ProdutoCtrl as vm'
            })
            .state ('produtos.main.novo', {
                url: '/novo',
                templateUrl: 'views/produtos/novo.html',
                //controller: 'ProdutoCtrl as vm',
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/fileinput/fileinput.min.js',
                                ]
                            }
                        ])
                    }
                }
            })

            .state ('categorias', {
                url: '/categorias',
                templateUrl: 'views/produtos/categorias/index.html',
                //controller: 'ProdutoCtrl as vm'
            })
            .state ('categorias.nova', {
                url: '/nova',
                templateUrl: 'views/produtos/categorias/nova.html',
                //controller: 'ProdutoCtrl as vm',
            })

    });