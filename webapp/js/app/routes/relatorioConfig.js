angular.module('materialAdmin')
.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        //------------------------------
        // RELATÓRIOS
        //------------------------------
        .state ('relatorios', {
            url: '/relatorios',
            templateUrl: 'views/common-2.html'
        })
        .state ('relatorios.main', {
            url: '/main',
            templateUrl: 'views/relatorios/index.html',
            controller:'RelatorioCtrl as vm',
            resolve: {
                loadPlugin: function($ocLazyLoad) {
                    return $ocLazyLoad.load ([
                        {
                            name: 'vendors',
                            insertBefore: '#app-level-js',
                            files: [
                                'js/app/modulos/relatorio/relatorioController.js',
                                'js/app/modulos/relatorio/relatorioFactory.js'
                            ]
                        },
                        {
                            name: 'vendors',
                            files: [
                                'vendors/input-mask/input-mask.min.js'
                            ]
                        }
                    ])
                }
            }
        })


    });
