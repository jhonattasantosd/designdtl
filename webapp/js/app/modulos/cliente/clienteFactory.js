angular.module('materialAdmin').factory("ClienteFactory",ClienteFactory);

function ClienteFactory($resource) {


    return {
        dados : $resource("http://simulado.cotacoesecompras.com.br/webservice/gerencia-usuario/:id",{id: '@id'}),
        desativarUsuario:$resource("http://simulado.cotacoesecompras.com.br/webservice/gerencia-usuario/desativar-usuario/:id",{id: '@id'},{'update': { method:'PUT' }}),
        ativarUsuario:$resource("http://simulado.cotacoesecompras.com.br/webservice/gerencia-usuario/ativar-usuario/:id",{id: '@id'},{'update': { method:'PUT' }})
    }
}