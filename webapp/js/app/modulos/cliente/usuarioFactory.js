angular.module('materialAdmin').factory("UsuarioFactory",UsuarioFactory);

function UsuarioFactory($resource) {

    return {
        getUsuario : $resource("http://simulado.cotacoesecompras.com.br/webservice/gerencia-usuario/usuario/:id",{id: '@id'}),
        putUsuario: $resource("http://simulado.cotacoesecompras.com.br/webservice/gerencia-usuario/usuario/:id",{id: '@id'},{'update': { method:'PUT',data:{}, isArray: false }}),

        getCargoUsuario: $resource("http://simulado.cotacoesecompras.com.br/webservice/gerencia-usuario/usuario-cliente/:id",{id: '@id'}),
        putCargoUsuario: $resource("http://simulado.cotacoesecompras.com.br/webservice/gerencia-usuario/usuario-cliente/:id",{id: '@id'},{'update': { method:'PUT',data:{}, isArray: false }}),
    }
}