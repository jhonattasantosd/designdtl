angular.module('materialAdmin').controller("ClienteCtrl",ClienteCtrl);

function ClienteCtrl(ClienteFactory) {
   
    carregaUsusarios();
    //desativeButtons(true);

    var vm = this;
    vm.listaUsuarios = [];
    vm.usuarioSelecionado = null;
    //paginacao
    vm.pageSize = 7;
    vm.currentPage = 1;
    vm.maxSize = 7;

    vm.nome = "Um texto";
    vm.login = "login";
    vm.email = "email@email.com";
    vm.cpf = "999888777-55";
    vm.datanasc = "24/05/2000";
    vm.editar = 0;
    vm.save = function(item, message) {
        if(item === 'editarUsuario') {
            this.editar = 0;
        }
    }

    vm.ativarUsuario = function (usuario) {
        var id = usuario.id;
        ClienteFactory.ativarUsuario.update({id:id},function (data) {
            carregaUsusarios();
        });
    }

    vm.desativarUsuario = function (usuario) {
        var id = usuario.id;
        ClienteFactory.desativarUsuario.update({id:id},function (data) {
            carregaUsusarios();
        });
    }

    function carregaUsusarios() {
        ClienteFactory.dados.query({id: 4804734}, function (data) {
            vm.listaUsuarios = data;
        });
    }
    function desativeButtons(value){
        vm.editarUsuario = value;
        vm.ativarUsuario = value;
        vm.desativarUsuario = value;
        vm.deletarUsuario = value;
        vm.permissoesUsuario = value;
    }
    function ativaDesativaUsuario(ativa,desativa) {
        vm.ativarUsuario = ativa;
        vm.desativarUsuario = desativa;
    }
    function ativaBtnEditarUsuario() {
        vm.editarUsuario = false;
    }
    function ativaBtnDeletarUsuario() {
        vm.deletarUsuario = false;
    }
    function ativaBtnPermissaoUsuario() {
        vm.permissoesUsuario = false;
    }

}