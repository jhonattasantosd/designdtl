angular.module('materialAdmin').controller("UsuarioCtrl",UsuarioCtrl);

function UsuarioCtrl(UsuarioFactory, $state,growlService,$stateParams) {

    var vm = this;

    vm.usuario = {};
    vm.profissional = {};
    vm.editarInfoBasica = 0;
    vm.editarInfoProfissional = 0;
    vm.editarInfoContato = 0;

    vm.salvar = function(item, message){

        if(item === 'editarInfoBasica') {
            this.editarInfoBasica = 0;
            atualizarInfoBasica();
        }
        if(item === 'editarInfoProfissional') {
            this.editarInfoProfissional = 0;
            atualizarInfoProfissional();
        }

        //growlService.growl(message+' Salvas com Sucesso!', 'inverse');
    };

    if($stateParams.id){

        UsuarioFactory.getUsuario.get({id:$stateParams.id}, function(data){
            vm.usuario = data;
        });

        UsuarioFactory.getCargoUsuario.get({id:$stateParams.id}, function(data){
            vm.profissional = data;
        });
    }


    function atualizarInfoBasica() {
        UsuarioFactory.putUsuario.update({id: $stateParams.id}, vm.usuario, function (data) {
            console.log(data);
            growlService.growl('Atualização '+data.nome+' Salvo com sucesso!', 'success');
        }, function(error){
            growlService.growl('Não foi possivel fazer a atualização!', 'danger');
        });
    }
    
    function atualizarInfoProfissional() {
        UsuarioFactory.putCargoUsuario.update({id: $stateParams.id}, vm.profissional, function (data) {
            console.log(data);
        });
    }


}