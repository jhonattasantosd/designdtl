angular.module('materialAdmin').controller("RelatorioCtrl",RelatorioCtrl);

function RelatorioCtrl(RelatorioFactory,ngTableParams,$filter,$scope) {
    var vm = this;
    var btn_periodo_fixo = angular.element("#btn_periodo_fixo button");
    var btn_sete_dias = angular.element("#btnSeteDias");
    var definir_datas = angular.element("#definir_datas input");
    var btn_definir_data = angular.element("#btn_definir_data");

    btn_periodo_fixo.addClass("btn btn-default");
    btn_sete_dias.addClass('btn-primary');

    vm.relatorio = {
        tipo : "pedido"
    }

    $scope.$watch('vm.relatorio.tipo', function(value) {
        if(value == 'pedido'){
            ativarBtnSeteDia()
            graficoPedidoSemana();
        }
        if(value == 'saving'){
            ativarBtnSeteDia()
            graficoSavingSemana();
        }
        if(value == 'fornecedor'){
            ativarBtnSeteDia()
            graficoFornecedorSemana();
        }
        if(value =='contrato'){
            ativarBtnSeteDia();
            graficoContratoSemana();
        }

    });


    vm.personalizar_periodo = false;

    vm.mostrarGraficoPedido = function () {
        return vm.personalizar_periodo == false && vm.relatorio.tipo == 'pedido';
    }
    vm.mostrarGraficoSaving = function () {
        return vm.personalizar_periodo == false && vm.relatorio.tipo == 'saving';
    }
    vm.mostrarGraficoFornecedor = function () {
        return vm.personalizar_periodo == false && vm.relatorio.tipo == 'fornecedor';
    }
    vm.mostrarGraficoContrato = function () {
        return vm.personalizar_periodo == false && vm.relatorio.tipo == 'contrato';
    }


    vm.personalizar = function (vaiPersonalizar) {
        if(vaiPersonalizar == true){
            btn_periodo_fixo.attr("disabled","disabled");
            btn_definir_data.attr("disabled", false);
            definir_datas.attr("disabled",false);
        }else{
            btn_periodo_fixo.attr("disabled",false);
            btn_definir_data.attr("disabled", true);
        }
    }
    
    vm.seteDias = function (event,tipoRelatorio) {
        ativarBtn(event);
        if(tipoRelatorio == 'pedido'){
            graficoPedidoSemana();
            vm.mostrarGraficoPedido();
        }
        if(tipoRelatorio == 'saving'){
            graficoSavingSemana();
            vm.mostrarGraficoSaving();
        }
        if(tipoRelatorio == 'fornecedor')
        {
            graficoFornecedorSemana();
            vm.mostrarGraficoFornecedor();
        }
        if(tipoRelatorio == 'contrato')
        {
            graficoContratoSemana();
            vm.mostrarGraficoContrato();
        }


    }
    vm.mesAtual = function (event,tipoRelatorio) {
        ativarBtn(event);
        if(tipoRelatorio == 'pedido') {
            graficoPedidoMes();
            vm.mostrarGraficoPedido();
        }
        if(tipoRelatorio == 'saving'){
            graficoSavingMes();
            vm.mostrarGraficoSaving();
        }
        if(tipoRelatorio == 'fornecedor')
        {
            graficoFornecedorMes();
            vm.mostrarGraficoFornecedor();
        }
        if(tipoRelatorio == 'contrato')
        {
            graficoContratoMes();
            vm.mostrarGraficoContrato();
        }

    }
    vm.mesPassado =  function (event,tipoRelatorio) {
        ativarBtn(event);
        if(tipoRelatorio == 'pedido') {
            graficoPedidoMesPassado();
            vm.mostrarGraficoPedido();
        }
        if(tipoRelatorio == 'saving') {
            graficoSavingMesPassado();
            vm.mostrarGraficoSaving();
        }
        if(tipoRelatorio == 'fornecedor')
        {
            graficoFornecedorMesPassado();
            vm.mostrarGraficoFornecedor();
        }
        if(tipoRelatorio == 'contrato')
        {
            graficoContratoMesPassado();
            vm.mostrarGraficoContrato();
        }
    }
    vm.seisMeses = function (event,tipoRelatorio) {
        ativarBtn(event);
        if(tipoRelatorio == 'pedido') {
            graficoPedidoSeisMeses();
        }
        if(tipoRelatorio == 'saving') {
            graficoSavingSeisMeses();
            vm.mostrarGraficoSaving();
        }
        if(tipoRelatorio == 'fornecedor')
        {
            graficoFornecedorSeisMeses();
            vm.mostrarGraficoFornecedor();
        }
        if(tipoRelatorio == 'contrato')
        {
            graficoContratoSeisMeses();
            vm.mostrarGraficoContrato();
        }
    }

    var graficoPedidoSeisMeses = function() {
        vm.labels = ["Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro"];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [6500, 5009, 8000, 2001, 5600, 3500]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };

        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoPedidoMesPassado = function() {
        vm.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [0, 5009, 8000, 2001, 5600, 0, 0,6500, 5009, 8000, 2001, 5600, 6500, 0,0, 5009, 8000, 2001, 5600, 0, 0,6500, 5009, 8000, 2001, 5600, 6500, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoPedidoMes = function() {
        vm.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [0, 5009, 8000, 2001, 5600, 0, 0,6500, 5009, 8000, 2001, 5600, 6500, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoPedidoSemana = function() {
        vm.labels = ["Segunda", "Terca", "Quarta", "Quinta", "Sexta", "Sabado", "Domingo"];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [6500, 5009, 8000, 2001, 5600, 0, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }

    var graficoSavingSemana = function() {
        vm.labels = ["Segunda", "Terca", "Quarta", "Quinta", "Sexta", "Sabado", "Domingo"];
        vm.series = ['Saving 25648'];
        vm.data = [
            [1000, 2000, 2100, 1500, 3000, 0, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoSavingMesPassado = function() {
        vm.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [0, 30, 40, 40, 50, 0, 0,85, 95, 65, 25, 89, 6500, 0,0, 5009, 8000, 2001, 5600, 0, 0,6500, 5009, 8000, 2001, 5600, 6500, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoSavingMes = function() {
        vm.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [0, 500, 600, 200, 560, 0, 0,600, 500, 800, 201, 560, 650, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoSavingSeisMeses = function() {
        vm.labels = ["Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro"];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [650, 509, 800, 200, 560, 350]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };

        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }

    var graficoFornecedorSeisMeses = function() {
        vm.labels = ["Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro"];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [6500, 5009, 8000, 2001, 5600, 3500]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };

        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoFornecedorMesPassado = function() {
        vm.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [0, 5009, 8000, 2001, 5600, 0, 0,6500, 5009, 8000, 2001, 5600, 6500, 0,0, 5009, 8000, 2001, 5600, 0, 0,6500, 5009, 8000, 2001, 5600, 6500, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoFornecedorMes = function() {
        vm.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [0, 5009, 8000, 2001, 5600, 0, 0,6500, 5009, 8000, 2001, 5600, 6500, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoFornecedorSemana = function() {
        vm.labels = ["Segunda", "Terca", "Quarta", "Quinta", "Sexta", "Sabado", "Domingo"];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [6500, 5009, 8000, 2001, 5600, 0, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }

    var graficoContratoSemana = function() {
        vm.labels = ["Segunda", "Terca", "Quarta", "Quinta", "Sexta", "Sabado", "Domingo"];
        vm.series = ['Saving 25648'];
        vm.data = [
            [1000, 2000, 2100, 1500, 3000, 0, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoContratoMesPassado = function() {
        vm.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [0, 30, 40, 40, 50, 0, 0,85, 95, 65, 25, 89, 6500, 0,0, 5009, 8000, 2001, 5600, 0, 0,6500, 5009, 8000, 2001, 5600, 6500, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoContratoMes = function() {
        vm.labels = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [0, 500, 600, 200, 560, 0, 0,600, 500, 800, 201, 560, 650, 0]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };
        //vm.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }
    var graficoContratoSeisMeses = function() {
        vm.labels = ["Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro"];
        vm.series = ['Pedido 25648'];
        vm.data = [
            [650, 509, 800, 200, 560, 350]
        ];
        vm.onClick = function (points, evt) {
            console.log(points, evt);
        };

        vm.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
    }

    var ativarBtn = function (event) {
        btn_periodo_fixo.attr("class","btn btn-default");
        var esseBtn = event.target;
        $(esseBtn).removeClass("btn-default");
        $(esseBtn).addClass("btn-primary");
    }
    var ativarBtnSeteDia = function () {
        btn_periodo_fixo.attr("class","btn btn-default");
        btn_sete_dias.removeClass("btn-default");
        btn_sete_dias.addClass('btn-primary');
    }

    vm.dataFaker = [
        {
        "fornecedor": "3M DO BRASIL LTDA",
        "status" : "A",
        "data_criacao": " 12/05/2016 às 14:25",
        "data_envio" : "12/05/2016 às 15:35 ",
        "prazo_resposta": "20/05/2016 às 18:00 ",
        "itens" : 3,
        "valor_pedido" : "R$ 1.100,00"
        },
        {
            "fornecedor": "COMFERRAL",
            "status" : "C",
            "data_criacao": "18/05/2016 às 14:50",
            "data_envio" : "18/05/2016 às 15:20  ",
            "prazo_resposta": "18/05/2016 às 15:20  ",
            "itens" : 2,
            "valor_pedido" : "R$ 2.100,00"
        },
        {
            "fornecedor": "3M DO BRASIL LTDA",
            "status" : "A",
            "data_criacao": " 12/05/2016 às 14:25",
            "data_envio" : "12/05/2016 às 15:35 ",
            "prazo_resposta": "20/05/2016 às 18:00 ",
            "itens" : 3,
            "valor_pedido" : "R$ 1.100,00"
        },
        {
            "fornecedor": "3M DO BRASIL LTDA",
            "status" : "A",
            "data_criacao": " 12/05/2016 às 14:25",
            "data_envio" : "12/05/2016 às 15:35 ",
            "prazo_resposta": "20/05/2016 às 18:00 ",
            "itens" : 3,
            "valor_pedido" : "R$ 1.100,00"
        },
    ];
    var data = vm.dataFaker;
    vm.tableSorting = new ngTableParams({
        page: 1,
        count: 10,
        sorting:{
            name:'asc'
        }

    }, {
        total: data.length, // length of data
        getData: function ($defer, params) {

            // use build-in angular filter
            var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;

            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });


    var init = function () {
        //graficoPedidoSemana();
        btn_definir_data.attr("disabled", true);
    }
    init();
}