var materialAdmin = angular.module('materialAdmin', [
    'satellizer',
    'ngAnimate',
    'ngResource',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'oc.lazyLoad',
    'nouislider',
    'ngTable',
    "checklist-model",
    'chart.js'
])

.constant('API_ENDPOINT', {
    url: 'http://localhost:8000/'
    //  For a simulator use: url: 'http://127.0.0.1:8080/api'
})

angular.module('materialAdmin').controller('loginCtrl', function($auth,$location){
    var vm = this;
    vm.login = 1;
    vm.register = 0;
    vm.forgot = 0;
    vm.usuario = {
        email: "harley.hickle@example.org",
        password: "secret"
    }


    vm.logar = function (usuario) {
        $auth.login(usuario)
            .then(function (res) {
                console.log(res);
                //$localStorage.token = res.data.token;
                //$localStorage.localStorage.setItem('token',res.data.token);
                //console.log($window.localStorage);
                //$location.path('/home');
                $location.path("home");
            })
            .catch(function (res) {
                console.log(res);
            });
    }
});

angular.module('materialAdmin')
    .factory("AuthInterceptor", function ($rootScope, $q, $window,$localStorage) {
        var requestInterceptor = {
            request: function(config) {
                // config.headers['Authorization'] = 'Bearer '+ $window.localStorage.yourTokenKey;
                config.headers = config.headers || {};
                if ($localStorage.token) {
                    console.log($localStorage.token);
                    config.headers.Authorization = 'Bearer ' + $localStorage.token;
                }

                return config;
            },
            requestError: function(rejectReason) {
                console.log(rejectReason);
            },
            response: function(response) {
                response.config.responseTimestamp = new Date().getTime();
                //console.log(response);
                return response;
            },
            responseError: function(response) {
                if(response.status == "401"){

                    console.log("error");
                }
                return $q.reject(response);
            }
        };

        return requestInterceptor;
    });

angular.module('materialAdmin').config(function ($httpProvider, $authProvider,API_ENDPOINT) {
    $authProvider.loginUrl = API_ENDPOINT.url+'api/auth/login';
    // $authProvider.signupUrl = API_ENDPOINT.url+'/auth/signUp';
    $authProvider.tokenName = 'token';
    $authProvider.tokenPrefix = '';
    $httpProvider.interceptors.push('AuthInterceptor');
})

angular.module('materialAdmin').run(function ($rootScope,$location,$auth) {
     $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
         var html = angular.element('html');
         var body = angular.element('body');
         if(!$auth.isAuthenticated()){
             if(next.name !== 'login'){
                 event.preventDefault();
                 console.log(next.name);
                 $location.path('login');
             }
         }else{
             html.removeClass('login-content');
             body.removeClass('login-content');
             if(next.name == 'login'){
                 console.log(next.name);
                 $location.path('home');
             }
         }
    })
})
