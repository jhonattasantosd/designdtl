<?php

namespace App\Http\Controllers;

use App\src\Repositories\UsuarioRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class UsuarioController extends Controller
{
    /**
     * @var UsuarioRepository
     */
    private $usuarioRepository;

    public function __construct(UsuarioRepository $usuarioRepository)
    {
        $this->usuarioRepository = $usuarioRepository;
    }

    public function index()
    {
        return $this->usuarioRepository->all();
    }
}
