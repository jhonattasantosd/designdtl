<?php

namespace App\src\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsuarioRepository
 * @package namespace App\src\Repositories;
 */
interface UsuarioRepository extends RepositoryInterface
{
    //
}
