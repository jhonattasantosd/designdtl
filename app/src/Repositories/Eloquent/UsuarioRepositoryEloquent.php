<?php

namespace App\src\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\src\Repositories\UsuarioRepository;
use App\src\Models\Usuario;
use App\src\Validators\UsuarioValidator;

/**
 * Class UsuarioRepositoryEloquent
 * @package namespace App\src\Repositories\Eloquent;
 */
class UsuarioRepositoryEloquent extends BaseRepository implements UsuarioRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Usuario::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
